const decoder = new TextDecoder("utf-8");
const encoder = new TextEncoder("utf-8");

export function Connect() {
  let socket = new WebSocket("ws://localhost:8080/connect");
  socket.binaryType = 'arraybuffer';
  return socket;
}

// export function WebSocketTest() {
//       // Let us open a web socket
//       var ws = new WebSocket("ws://localhost:8080/connect");
//
//       ws.binaryType = 'arraybuffer';
//
//       ws.onopen = function() {
//
//          // Web Socket is connected, send data using send()
//          // ws.send("Will Whitmey");
//          // console.log("Message is sent...");
//       };
//
//       ws.onmessage = function (evt) {
//          let message = JSON.parse(decoder.decode(evt.data));
//          console.log(message);
//       };
//
//       // ws.onclose = function() {
//       //
//       //    // websocket is closed.
//       //    console.log("Connection is closed...");
//       // };
// }

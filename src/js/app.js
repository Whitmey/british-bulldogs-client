import './style.scss';
import { Connect } from './js/network';
const decoder = new TextDecoder("utf-8");

let socket = Connect("Will");

socket.onmessage = function (evt) {
   let message = JSON.parse(decoder.decode(evt.data));
   console.log(message);
   switch(message.type) {
    case 'CONNECTED':
      startGame(message.data.x, message.data.y);
      break;
    case "Orange":
      text = "I am not a fan of orange.";
      break;
    default:
      let text = "I have never heard of that fruit...";
  }
};

let myGamePiece;

var myGameArea = {
  canvas : document.createElement("canvas"),
  start : function() {
    this.canvas.width = 1500;
    this.canvas.height = 750;
    this.context = this.canvas.getContext("2d");
    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    this.interval = setInterval(updateGameArea, 20); // Update for performance, don't use setInverval
    window.addEventListener('keydown', function (e) {
      myGameArea.keys = (myGameArea.keys || []);
      myGameArea.keys[e.keyCode] = true;
    })
    window.addEventListener('keyup', function (e) {
      myGameArea.keys[e.keyCode] = false;
    })
  },
  clear : function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
}

function component(width, height, color, x, y) {
  this.width = width;
  this.height = height;
  this.speedX = 0;
  this.speedY = 0;
  this.x = x;
  this.y = y;
  this.update = function(){
    let ctx = myGameArea.context;
    ctx.fillStyle = color;
    ctx.fillRect(this.x, this.y, this.width, this.height);
  }
  this.newPos = function() {
    this.x += this.speedX;
    this.y += this.speedY;
  }
}

function updateGameArea() {
  myGameArea.clear();
  myGamePiece.speedX = 0;
  myGamePiece.speedY = 0;
  if (myGameArea.keys && myGameArea.keys[37]) {myGamePiece.speedX = -5; }
  if (myGameArea.keys && myGameArea.keys[39]) {myGamePiece.speedX = 5; }
  if (myGameArea.keys && myGameArea.keys[38]) {myGamePiece.speedY = -5; }
  if (myGameArea.keys && myGameArea.keys[40]) {myGamePiece.speedY = 5; }
  myGamePiece.newPos();
  myGamePiece.update();
}

function startGame(x, y) {
  myGameArea.start();
  myGamePiece = new component(20, 20, "black", x, y);
}

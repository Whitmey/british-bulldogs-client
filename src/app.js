import './style.scss';
import { Connect, SendMessage } from './js/network';
const decoder = new TextDecoder("utf-8");
const encoder = new TextEncoder("utf-8");
var gameState;

var socket = Connect();

socket.onmessage = function(evt) {
   let message = JSON.parse(decoder.decode(evt.data));
   switch(message.type) {
    case 'CONNECTED':
      gameState = message.data.game;
      startGame(message.data.id, message.data.x, message.data.y);
      break;
    case "UPDATE_PLAYERS":
      gameState.players = message.data;
      break;
    default:
      let text = "I have never heard of that fruit...";
  }
};

window.addEventListener('beforeunload', function (e) {
  socket.send(encoder.encode('closing')) // TODO messageType: closing, gameId: '123'
});

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var width;
var height;
var myPlayer;

var resize = function() {
  canvas.width = 1500;
  canvas.height = 750;
};
window.onresize = resize;
resize();

ctx.fillStyle = 'black'

function Player(id, x, y) {
  this.id = id;
  this.x = x,
  this.y = y,
  this.pressedKeys = {
    left: false,
    right: false,
    up: false,
    down: false
  },
  this.update = function() {
    if (this.pressedKeys.left) {
      if (this.x - 10 > 0) {
        this.x -= 10;
      }
    }
    if (this.pressedKeys.right) {
      if (this.x + 10 < canvas.width - 20) {
        this.x += 10
      }
    }
    if (this.pressedKeys.up) {
      if (this.y - 10 > 0) {
        this.y -= 10
      }
    }
    if (this.pressedKeys.down) {
      if (this.y + 10 + 20 < canvas.height) {
        this.y += 10
      }
    }
    // Maybe use a counter so it only updates every 5 frames? Consider myPlayer being in list of gamePlayers. So being painted twice...
    socket.send(encoder.encode(JSON.stringify({
      "type": "MOVEMENT",
      "data": {
        "id": this.id,
        "gameId": gameState.id,
        "x": this.x,
        "y": this.y
      }
    })));
  },
  this.draw = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillRect(this.x, this.y, 20, 20)
  }
}

function OtherPlayer(x, y) {
  this.x = x,
  this.y = y,
  this.draw = function() {
    ctx.fillRect(this.x, this.y, 20, 20)
  }
}

var keyMap = {
  68: 'right',
  65: 'left',
  87: 'up',
  83: 'down'
}
function keydown(event) {
  var key = keyMap[event.keyCode]
  myPlayer.pressedKeys[key] = true
}
function keyup(event) {
  var key = keyMap[event.keyCode]
  myPlayer.pressedKeys[key] = false
}

window.addEventListener("keydown", keydown, false)
window.addEventListener("keyup", keyup, false)

function updateOtherPlayers() {
  for (var key in gameState.players) {
    if (!gameState.players.hasOwnProperty(key)) continue;
    let player = gameState.players[key];
    new OtherPlayer(player.x, player.y).draw();
  }
}

function loop() {
  myPlayer.update();
  myPlayer.draw();
  updateOtherPlayers();
  window.requestAnimationFrame(loop);
};

function startGame(playerId, x, y, gameState) {
  myPlayer = new Player(playerId, x, y);
  gameState = gameState;
  loop();
}
